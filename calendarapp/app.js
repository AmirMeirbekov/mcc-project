// Google OAuth Configuration
var googleConfig = {
  clientID: '676002711810-q62grbodpeokic3ub21eg98kfj10fl1b.apps.googleusercontent.com',
  clientSecret: 'Ut2354QNov89nP_duCZDlVI7',
  calendarId: 'sjb1qhbicuu3u0kl6suat53h8c@group.calendar.google.com',
  redirectURL: 'http://localhost:2002/auth'
};

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var urladdr = "http://127.0.0.1:3000"  // hardcoded url address - it will be passed to Views
var jwt = require('jwt-simple');

// Now definitions for routes that handle our REST API:
//var routes = require('./routes/index') ;
var users = require('./routes/users') ;
//var event = ;
//var calendar = require('./routes/calendar');
var evs = require('./routes/events');
var auth = require('./routes/auth')
//var login = require('./routes/login')

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'client'));
//app.set('view engine', 'html');

// Middleware configuration:
app.use(favicon(path.join(__dirname, 'client', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(cookieParser('3CCC4ACD-6ED1-4844-9217-82131BDCB239'));
app.use(session({secret: '2C44774A-D649-4D44-9535-46E296EF984F', resave: true, saveUninitialized: true}));
app.use(express.static(path.join(__dirname, 'client')));
//app.use(express.static(path.join(__dirname, 'client/angular')));

/* Send authentication status to the template pages (OBSOLETE):
app.use(function(req, res, next) {
  if (req.session && req.session.logged)
    res.locals.logged = true;
  next();
});
*/

/* This is for sessions:
app.use(function(req, res, next) {
    res.locals.address = urladdr;
    console.log("THIS SESSION NOW: "+ req.session.id);
    console.log("Is user LOGGED: " + req.session.logged);
    console.log("The user is: " + req.session.user);
    next(); // Call the next middleware
}); */

// Associate requested pages with routes:
app.use('/api/auth', auth);
//app.use('/users', users);
app.use('/api/user', users);
//app.use('/logout', users);
//app.use('/api/singup', users);
//app.use('/events', evs);
//app.use('/api/calendar', calendar);   // first invoke authorization function, and then direct it to calendar route.
app.use('/api/events', evs);

app.get('*', function(req, res) {
        res.sendFile(__dirname + '/client/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

/* Sign and give a token to the user
app.post('/session', function(req,res){
            var username = req.body.username
            // TODO: VALIDATE PASSWORD HERE!
            var token = jwt.ecode({user-name: username}, secretKey)
            res.json(token)
            })

app.get('/user', function(req, res){
        var token = req.headers['x-auth']
        var user = jwt.decode(token, secretKey)
            // TODO: pull user info fromdata base
        res.json(user)
})


 */

/* catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
*/


/* error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
//app.use(function(err, req, res, next) {
//  res.status(err.status || 500);
//  res.render('error', {
//    message: err.message,
//    error: {}
//  });
//});

*/
module.exports = app;
