var jwt    = require('jwt-simple');
var config = require('../config');
mongoose = require('mongoose');

module.exports = function(req, res, callback) {
    console.log("Client requested something. We first have to check his token");
    if (!req.headers['x-auth']) {
        console.log(" ...NO TOKEN IN REQUEST OBJECT:"+JSON.stringify(req.headers));
        return res.send(401);
    }
     console.log(" ...He has got a token. Let us decode it...");
    var auth = jwt.decode(req.headers['x-auth'], config.secret);
    console.log(" ..It has been decoded. The decoded token is: " + JSON.stringify(auth));
    console.log("Looking now in the database for the user with the name:" + auth.username);
    mongoose.model('User').findOne({username: auth.username}, function (err, user) {
        if (err) {
            callback(err, null);
        }
        console.log("The user is found. Carrying on in normal mode.");
        console.log("The user is: "+JSON.stringify(user));
        callback(null, user);
        // We returned User object as json
    });
}