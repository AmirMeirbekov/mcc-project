var router = require('express').Router();
var jwt = require('jwt-simple');
var User = require('../model/User.js');
var config = require('../config');
var verifyToken = require('./verifyToken');



// get an existing user as JSON object {username, pass, mail}. Client has to have token already for this action.
router.get('/', function (req, res, next){
    verifyToken(req, res, function(err, data){
            if(err){
                return next(err);
            } else {
                console.log("TOKEN OBJ:" + JSON.stringify(data));
                var usr = data.username;
                User.findOne({username: usr}, function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    console.log("The user with name " + user.username + " found. Responding with the user object:" + JSON.stringify(user));
                    res.json(user);      // We returned User object as json
                });
            }
    });
});

// create a new user
router.post('/', function (req, res, next) {
    console.log("The client has submitted a post request to create a new user "+ req.body.username);
    var userProfile = req.body;
    console.log("The client has submitted a post request to create a new user: %j", userProfile);
    var user = new User({username: req.body.username})
    user.password = req.body.password;
    user.email = req.body.email;
    user.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send(201);
    })
});


router.route('/edit').put(function(req, res) {
    console.log("CLIENT TRIES TO UPDATE USER INFO");
    verifyToken(req, res, function(err, data){
        if(err){
            return next(err);
        } else { // token is good
            var update_dict = {};
            console.log("username submitted:"+data.username);
            console.dir(req);
            if (data.username) {
                update_dict.username = data.username;
            };
            if (req.body.email) {
                update_dict.email = req.body.email;
            };

            if (update_dict) {
                data.update(update_dict, function (err, userID) {
                    if (err) {
                        res.send("There was a problem updating the information to the database: " + err);
                    } else {
                        res.format({
                            // JSON responds showing the updated values
                            json: function () {   // we respond back with update_dict that has the updated data.
                                res.json(update_dict);
                            }
                        });
                    }
                });
            }
        }
    });
});

module.exports = router;