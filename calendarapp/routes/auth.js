var express = require('express');
var router = express.Router();
//User   = require('../model/user');
var jwt    = require('jwt-simple');
var config = require('../config');
mongoose = require('mongoose'); //mongo connection

/* do the following when the page /login is requested with GET method:
router.get('/', function(req, res, next) {
  res.render('login');
});
*/

// This API sends signed token to user given his username and password (LOGIN).
router.post('/', function(req, res, next) {
  console.log('Username of logging in User:' + req.body.username);
  console.log('Password of logging in User:' + req.body.password);
  mongoose.model('User').findOne({username: req.body.username})
      .select('password').select('username').exec(function (err, user){
        if (err) {
          console.log("ERRORRRRRRRRAA");
          return next(err);
        }
        if (!user) {
            return res.send(401);
          }
        // if we are here, then the user exists in the DB. Compare passwords now
        if (user.password === req.body.password) {
          // Password is good. Send the token:
          var token = jwt.encode({username: user.username}, config.secret);
          //send the token to the client:
          console.log("SENDING TOKEN TO THE CLIENT:"+token);
          res.send(token);
        }
        else {
          // username was correct, but password wasn't. Dismiss:
          return res.send(401);
        }

      })
});


/*router.post('/', function(req, res, next) {
  if (!req.body.username || !req.body.password)
    return res.json(201, {error: "Please enter your email and password."});
  mongoose.model('User').findOne({
    username: req.body.username,
    password: req.body.password
  }, function(error, user){
    if (error) return next(error);
    if (!user) return res.json(201, {error: "Incorrect email&password combination."});
    req.session.user = user; // change
    req.session.logged = true;// change
    res.redirect('/calendar');// change
  })
});





router.get('/logout', function(req, res, next){
  req.session.destroy();
  res.redirect('/');
});

// do the following when the page /login is requested with GET method:
router.get('/list', function(req, res, next) {
  console.log("List selected");
  mongoose.model('User').find({}, function (err, list_users) {
    if (err) {
      console.log("List error");
      res.render('login', {error: "No users found - or error occurred!"});
    } else {
        // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
        //res.format({
          // JSON response will show array of users, which are in JSON format
          //json: function(){
          //    res.json(users);
          //}
      //});
      console.log("List success");
      res.render('index', {error: list_users});
    }
  });
});

// do the following when the page /signup is requested with GET method:
router.get('/signup', function(req, res, next) {
  res.render('signup');
});

// do the following when the page /login is requested with GET method:
router.post('/signup', function(req, res, next) {
  if (!req.body.username || !req.body.passwd) {
    return res.render('signup', {error: "You haven't filled username and password!"});
  }
  var user = {
    name: req.body.username,
    pass: req.body.passwd,
    email: req.body.email
  };
  mongoose.model('User').create({username: user['name'], password:user['pass'], accessToken:"", email: user['email']},function(error, data) {
    if (error) return next(error);
    res.render('login', {error: "User has been created successfully.", logged: true});
  }
  );
});
*/
module.exports = router;
