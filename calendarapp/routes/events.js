var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    mEvent = require('../model/Events.js');
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    util = require('util'), // used for input validation purposes
    expressValidator = require('express-validator'); // used for input validation purposes

var User = require('../model/User.js');
var verifyToken = require('./verifyToken');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(expressValidator());
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))

// ========= Handlers for REST requests =========

// Retrieve all events from Mongo. this will be accessible from 
// http://127.0.0.1:3000/events if the default route for / is left unchanged
// ----- Request sample
// curl -X GET -H "Content-type: application/json" http://localhost:3000/events/
// ----- end
router.get('/', function(req, res, next) {
    verifyToken(req, res, function(err, data){
        if(err){
            return next(err);
        } else {
            console.log("TOKEN OBJ:" + JSON.stringify(data));
            var usr = data.username;
            User.findOne({username: usr}, function (err, userReturned) {
                if (err) {
                    return next(err);
                }
                //user.username
                mongoose.model('Event').find({user: userReturned.username}, function (err, events) {
                  if (err) {
                      return console.error(err);
                  } else {
                      // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
                      res.format({
                        // JSON response will show array of events, which are in JSON format
                        json: function(){
                            res.json(events);
                        }
                    });
                  }
                });
            });
        }
    });
    //---------
});
    
// Create new event
// ----- Request sample
// curl -X POST -H "Content-type: application/json" -d '{"name":"Test","loc":"Test","priority":"1","repeat":"true","availability":"active","sts":"active","date":"02.01.2016","time":"12:12"}' http://localhost:3000/events/new
// ----- end
router.post('/new', function(req, res) {
    console.log("NEW EVENT");
    if (req.accepts('json') === 'json') {
        // Check username from the token
            verifyToken(req, res, function(err, data){
            if(err){
                return next(err);
            } else {
                console.log("TOKEN OBJ:" + JSON.stringify(data));
                var usr = data.username;
                User.findOne({username: usr}, function (err, returnedUser) {
                    if (err) {
                        return next(err);
                    }
                    // Write your code here:
                          // ------- Validation of input data
                    console.log("JSON DATA RECEIVED:");
                      req.check({
                       'name': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        },
                        'loc': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        },
                        'repeat': {
                          notEmpty: true,
                          errorMessage: 'Required',
                          isBoolean: {errorMessage: 'Boolean required'},
                        },
                        'sts': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        },
                        'priority': {
                          notEmpty: true,
                          errorMessage: 'Required',
                          isInt: {errorMessage: 'Integer required'},
                        },
                        'availability': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        },
                        'date': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        },
                        'time': {
                          notEmpty: true,
                          errorMessage: 'Required',
                        }
                      });
                      var errors = req.validationErrors();
                      if (errors) {
                          console.log("WE HAVE AN ERROR VALIDATION!")
                        res.send(util.inspect(errors), 400);
                        return;
                      }
                      // ------- end of validation


                      // Get values from POST request.
                      var name = req.body.name;
                      // ------ setting time and date
                      var date = new Date(req.body.date);
                      var time = req.body.time;
                      var hours_minutes = time.match(/(\d+)[:\.](\d+)/);

                      if (!hours_minutes || !hours_minutes[1] || !hours_minutes[2]) {
                        res.json({error: 'Time should be in a format 00:00'});
                      }
                      date.setHours(parseInt(hours_minutes[1], 10));
                      date.setMinutes(parseInt(hours_minutes[2], 10));
                      // ------ end
                      var user = returnedUser.username; // <-----
                        var loc = req.body.loc;
                      var priority = req.body.priority;
                      var sts = req.body.sts;
                      var repeat = req.body.repeat;
                      var availability = req.body.availability;
                      // Call the create function for our database
                      mongoose.model('Event').create({
                          name: name,
                          date: date,
                          user: user, //  <-------
                          loc: loc,
                          priority: priority,
                          sts: sts,
                          repeat: repeat,
                          availability: availability,
                      }, function (err, event) {
                          if (err) {
                              res.send("There was a problem adding the information to the database.");
                          } else {
                              // Event has been created
                              console.log('POST creating new event: ' + event);
                              res.format({
                                // JSON response will show the newly created event
                                json: function(){
                                    res.json(event);
                                }
                            });
                          }
                      })

                });
            }
            });
    } else {
      res.json({error: 'No data send or data is not json'});
      return;
    }
});

// route middleware to validate :id
router.param('id', function(req, res, next, id) {
    // find the ID in the Database
    mongoose.model('Event').findById(id, function (err, event) {
        // if error occurs
        if (err) {
          return next(err);
        // if it is not found - continue on
        }
        if (!event) {
          res.json({error: 'Not found'});
          return;
        }
        // if it is found
        req.id = id;
        // go to the next middleware
        next(); 
    });
});

// Retrieve event with given id
// ----- Request sample
// curl -X GET -H "Accept: application/json" http://localhost:3000/events/:id
// ----- end
router.route('/:id')
  .get(function(req, res) {
    mongoose.model('Event').findById(req.id, function (err, event) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
        // console.log('GET Retrieving ID: ' + event._id);
        res.format({
          json: function(){
              res.json(event);
          }
        });
      }
    });
  });

// Edit the event with given id - update, delete
router.route('/:id/edit')
	// PUT request to update a event with given id
  // ----- Request sample
  // curl -X PUT -H "Content-type: application/json" -H "Accept: application/json" -d '{"name":"Test","loc":"Test","priority":"1","repeat":"true","availability":"active","sts":"active","date":"02.01.2016","time":"12:13"}' http://localhost:3000/events/:id/edit
  // ----- end
	.put(function(req, res) {
        console.log("I AM UPDATING THIS EVENT NOW");
	  verifyToken(req, res, function(err, data){
            if(err){
                return next(err);
            } else {
                console.log("TOKEN OBJ:" + JSON.stringify(data));
                var usr = data.username;
                User.findOne({username: usr}, function (err, returnedUser) {
                    if (err) {
                        return next(err);
                    }
                    // Write your code here:
                    	  // Retrieve values from request body and put them into update_dict
      var update_dict = {};
      if (req.body.name) {
        update_dict.name = req.body.name;
      };
        update_dict.user = returnedUser.username;
      if (req.body.loc) {
        update_dict.loc = req.body.loc;
      };
      if (req.body.priority) {
        update_dict.priority = req.body.priority;
      };
      if (req.body.sts) {
        update_dict.sts = req.body.sts;
      };
      if (req.body.repeat) {
        update_dict.repeat = req.body.repeat;
      };
      if (req.body.availability) {
        update_dict.availability = req.body.availability;
      };
      if (req.body.name) {
        update_dict.name = req.body.name;
      };
      if (req.body.date && req.body.time) {
        // ------ setting time and date
        var date = new Date(req.body.date);
        var time = req.body.time;
        var hours_minutes = time.match(/(\d+)[:\.](\d+)/);
        if (!hours_minutes || !hours_minutes[1] || !hours_minutes[2]) {
          res.json({error: 'Time should be in a format 00:00'});
          return;
        }
        date.setHours(parseInt(hours_minutes[1], 10));
        date.setMinutes(parseInt(hours_minutes[2], 10));
        update_dict.date = date;
        // ------ end
      };
      if (!req.body.date && req.body.time) {
        res.json({error: 'Send time and date'});
        return;
      };
      if (req.body.date && !req.body.time) {
        res.json({error: 'Send time and date'});
        return;
      };

	    // find the document by id
	    mongoose.model('Event').findById(req.id, function (err, event) {
	        // update it
          if (update_dict) {
  	        event.update(update_dict, function (err, eventID) {
  	          if (err) {
  	              res.send("There was a problem updating the information to the database: " + err);
  	          }
  	          else {
                  res.format({
                     // JSON responds showing the updated values
                    json: function(){
                        res.json({
                            message : 'updated',
                            id : event._id
                         });
                     }
                  });
  	           }
  	        });
          } else {
            res.json({error: 'Send data to update'});
            return;
          }
	    });

                });
            }
            });
	})
	// DELETE a Event by id
  // ----- Request sample
  // curl -X DELETE -H "Accept: application/json" http://localhost:3000/events/:id/edit
  // ----- end
	.delete(function (req, res){
	    // find event by id
        console.log("I AM DELETING THIS EVENT NOW");
	    mongoose.model('Event').findById(req.id, function (err, event) {
	        if (err) {
	            return console.error(err);
	        } else {
	            // remove it from Mongo
	            event.remove(function (err, event) {
	                if (err) {
	                    return console.error(err);
	                } else {
	                    // Returning success messages saying it was deleted
	                    console.log('DELETE removing ID: ' + event._id);
	                    res.format({
	                         // JSON returns the item with the message that is has been deleted
	                        json: function(){
                               res.json({
                                  message : 'deleted',
                                  id : event._id
                               });
	                         }
	                      });
	                }
	            });
	        }
	    });
	});

router.delete('/deleteall', function(req, res) {
  // ----- Request sample
  // curl -X DELETE -H "Accept: application/json" http://localhost:3000/events/deleteall
  // ----- end
  mongoose.model('Event').remove({}, function (err, removed) {
      if (err) {
          return console.error(err);
      } else {
          // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
          res.format({
            // JSON response will show array of events, which are in JSON format
            json: function(){
                res.json({
                  message : 'deleted_all_data',
               });
            }
        });
      }     
    });
});

// --------- Retriving events in the given date range
router.param('start', function (req, res, next, start) {
  req.start = start;
  next();
})

router.param('end', function (req, res, next, end) {
  req.end = end;
  next();
})

router.get('/range/:start/to/:end', function(req, res) {
  // ----- Request sample
  // curl -X GET -H "Accept: application/json" http://localhost:3000/events/range/2016-01-10/to/2016-01-15
  // ----- end
  mongoose.model('Event').find({
    date: {
        $gte: new Date(req.start),
        $lt: new Date(req.end)
    }
  }, function (err, events) {
    if (err) {
        return console.error(err);
    } else {
        // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
        res.format({
          // JSON response will show array of events, which are in JSON format
          json: function(){
              res.json(events);
          }
      });
    }     
  });
});
// ---------

// --------- Retriving events with given status
router.param('status', function (req, res, next, status) {
  req.status = status;
  next();
})

router.get('/status/past', function(req, res) {
  // ----- Request sample
  // curl -X GET -H "Accept: application/json" http://localhost:3000/events/status/past
  // curl -X GET -H "Accept: application/json" http://localhost:3000/events/status/upcoming
  // ----- end
  mongoose.model('Event').find({
    sts: 'past'
  }, function (err, events) {
    if (err) {
        return console.error(err);
    } else {
        // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
        res.format({
          // JSON response will show array of events, which are in JSON format
          json: function(){
              res.json(events);
          }
      });
    }     
  });
});
router.get('/status/upcoming', function(req, res) {
  mongoose.model('Event').find({
    sts: 'upcoming'
  }, function (err, events) {
    if (err) {
        return console.error(err);
    } else {
        // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
        res.format({
          // JSON response will show array of events, which are in JSON format
          json: function(){
              res.json(events);
          }
      });
    }     
  });
});
// ---------

// --------- Retriving event basing on priority
router.param('from_priority', function (req, res, next, from_priority) {
  req.from_priority = from_priority;
  next();
})

router.param('to_priority', function (req, res, next, to_priority) {
  req.to_priority = to_priority;
  next();
})

router.get('/priority/:from_priority/to/:to_priority', function(req, res) {
  // ----- Request sample
  // curl -X GET -H "Accept: application/json" http://localhost:3000/events/priority/2/to/4
  // ----- end
  mongoose.model('Event').find({
    priority: {
        $gte: Number(req.from_priority),
        $lt: Number(req.to_priority)
    }
  }, function (err, events) {
    if (err) {
        return console.error(err);
    } else {
        // JSON response. JSON responses require 'Accept: application/json;' in the Request Header
        res.format({
          // JSON response will show array of events, which are in JSON format
          json: function(){
              res.json(events);
          }
      });
    }     
  });
});
// ---------

module.exports = router;
