/**
 * Created by Lukianchikov on 02-Nov-15.
 */
angular.module('CalendarApp.Signup')
    .controller('SignupCtrl', SignupCtrl);

function SignupCtrl($scope, $location, $rootScope, SenderService, $timeout) {
    var signup = this;

    $scope.signup = function(username, email, password1, password2) {
        console.log("SIGNUP PRESSED!!!!!!")
        if (password1 != password2){
            $rootScope.$broadcast('onCustomError', "Passwords do not match.");
        } else {
          var newUser = { username: username, email: email, password: password1 };
            SenderService.sendPost('/api/user', {}, angular.copy(newUser))
                .then(function (result) {
                    console.log("User "+username+" has been created!");
                    $rootScope.$broadcast('onCustomSuccess', "User "+username+" has been created!");
                    $timeout(function() {
                        $location.path('/login');
                        }, 5000);
                }, function (reason) {
                    $rootScope.$broadcast('onCustomError', reason.data);
                });
        }
    }
};