angular.module('CalendarApp.Login')
    .controller('LoginCtrl',
        function ($scope, $location, $window, LoginService, $rootScope) {
            var loginCtrl = this;

            $scope.login = function(username, password) {
                console.log("LOGIN PRESSED. TRYING TO LOGIN USING LOGIN SERVICE.....");
                LoginService.login(username, password)
                    .then(function (response) {    // success
                        console.log('LOGIN CONTROLLER AFTER SUCCESS IN LOGIN SERVICE: Logged in succesfully!;');
                        console.log('Response from LoginService was: '+response);
                        $window.localStorage.setItem('token',response);
                        LoginService.isLoggedIn = true;
                        //$rootScope.$broadcast('login', response.data)   // < -if we actually logged in
                        $location.path('/')
                }, function (err) {                 // failure   - broadcast our error event with the error message as
                                                        // args. Message Controller has listener for it to display.
                        $rootScope.$broadcast('onCustomError', err.data);
                    });
            };
        });