angular.module('CalendarApp.Common')
    .constant('CURRENT_BACKEND', 'node')
    //.constant('CURRENT_BACKEND', 'SOME_OTHER_BACKEND')
    .service('ServersideConfigService', function($rootScope, CURRENT_BACKEND) {
        var service = this,
            serversideMap = {
                node: { URI: 'http://127.0.0.1:3000/', root: 'api/', format: ''}
            },
            currentServerside = serversideMap[CURRENT_BACKEND],
            userId = '',
            backend = CURRENT_BACKEND;

        service.getUrl = function(model) {    // Get Url for the provided model
            return currentServerside.URI + currentServerside.root + userId + model;
        };

        service.getUrlForId = function(model, id) {
            return service.getUrl(model) + id + currentServerside.format;
        };

        service.getCurrentBackend = function() {
            return backend;
        };

        service.getCurrentFormat = function() {
            return currentServerside.format;
        };

        service.getCurrentURI = function() {
            return currentServerside.URI;
        };

        $rootScope.$on('onCurrentUserId', function(event, id){
            userId = id;
        });
    });