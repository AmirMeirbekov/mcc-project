// This Login Service is used to manage login state across other controllers.

angular.module('CalendarApp.Common')
    .service('LoginService',            // Registering  a service with CalendarApp.Common module.
        function ($http, $location, $rootScope, $window, SenderService) {
            var service = this;
            service.isLoggedIn = false;

            service.getUser = function () {   // this function returns json object of {username, email}
                return SenderService.sendGet('/api/user', {'x-auth': service.token}, {});
            }

            service.getLoggedProfile = function() {
                return service.profile;
            }

            service.setLoggedProfile = function(username, email) {
                service.profile = {'username': username, 'email': email};
            }

            service.setToken = function(token){
                SenderService.setToken(token);
            }

            service.login = function (username, password) {
                return SenderService.sendPost('/api/auth', {}, {'username': username, 'password': password}).then(function (val) {
                    service.token = val.data;       //this service saves token received after login as property
                    SenderService.setToken(val.data);
                    var response = service.token;
                    console.log("LOGIN SERVICE: Server responded with this:"+ response);
                    console.log("LOGIN SERVICE: Retrieving profile from the server using the token received...");
                    service.getUser().then(function (reply){
                                    // We got the user object from the server. It should have {id,username,__r} structure
                                    console.log("LOGIN SERVICE: WE JUST RETRIEVED THE PROFILE FOR THIS USER AND IT IS:"+JSON.stringify(reply.data));
                                    service.profile = reply.data;
                                    service.isLoggedIn = true;
                                    $rootScope.$broadcast('onCurrentUser', reply.data);// Dispatch an event onCurrentUserId
                                            // downwards to all child scopes (and their children) notifying the registered
                        }, function (error){
                                    // We didn't receive the user, there has been some error.

                        }
                    );
                    return response;
                });
            }

            service.authenticateUser = function () {
                console.log("Trying to authenticate the user");  // This service can be used if we want to know if we have a token
                if (service.isLoggedIn) {             // and if we do, then we check userprofile we store and broadcast it
                    var token = service.token;        // currently this function is used during page changes.
                    if (!token) {
                        $location.path('/login');
                    } else {
                        var profile = service.profile;
                        $rootScope.$broadcast('onCurrentUser', profile);
                    }
                } else {    // Not logged in User
                    if (!token) {    // And sanity check - he doesn't have a token
                        desiredPath = $location.path();
                        if (desiredPath != "/signup") {
                            $location.path('/login');
                        }
                    } else {
                        // He is not logged in but he has a token!
                        service.getUser().then(
                            function (profile){
                                // successfully found user profile.
                                 service.isLoggedIn = true;     // make it official that he is logged in.
                                 $rootScope.$broadcast('onCurrentUser', profile);   // tell to all other controllers.
                            },
                            function (err) {
                                // So he is not logged in, he has a token, and the token is wrong:
                                service.isLoggedIn = false;
                                $location.path('/login');
                                $rootScope.$broadcast('onCustomError', "Hey, your token is invalid!");
                            }
                        )
                    }
                }
            }

            service.logout = function() {    // logging out function.
                $window.localStorage.removeItem('token');
                SenderService.deleteToken();
                console.log("I just ordered to remove token from local storage. Is it still here? Value="+$window.localStorage.getItem('token'));
                service.isLoggedIn = false;
                return
            }


        })