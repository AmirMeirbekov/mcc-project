angular.module('CalendarApp.Common')
    .service('SenderService',            // Registering  a service with CalendarApp.Common module.
        function ($http, $window) {
            var sender = this;
            var address = 'http://127.0.0.1:3000';

            sender.sendPost = function(uri, myHeaders, body){
                if(!myHeaders['x-auth']){       // append token if we have it
                    var myToken = sender.token;
                    if (myToken){
                        myHeaders['x-auth'] = myToken;
                    }
                }
                var myUrl = address + uri;
                var req = {
                    method: 'POST',
                    url: myUrl,
                    headers: myHeaders,
                    data: body
                }
                return $http(req);
            }

            sender.sendGet = function(uri, myHeaders, body){
                if(!myHeaders['x-auth']){       // append token if we have it
                    var myToken = sender.token;
                    if (myToken){
                        myHeaders['x-auth'] = myToken;
                    }
                }
                var myUrl = address + uri;
                var req = {
                    method: 'GET',
                    url: myUrl,
                    headers: myHeaders,
                    data: body
                }
                return $http(req);
            }

            sender.sendPut = function(uri, myHeaders, body){
                console.log("headers"+JSON.stringify(myHeaders));
                console.log("BODY"+body.email);
                if(!myHeaders['x-auth']){       // append token if we have it
                    var myToken = sender.token;
                    if (myToken){
                        myHeaders['x-auth'] = myToken;
                    }
                }
                var myUrl = address + uri;
                 var req = {
                    method: 'PUT',
                    url: myUrl,
                    headers: myHeaders,
                    data: body
                }
                console.log(JSON.stringify(req))
                return $http(req);
            }

            sender.sendDel = function(uri, myHeaders, body){
                if(!myHeaders['x-auth']){       // append token if we have it
                    var myToken = sender.token;
                    if (myToken){
                        myHeaders['x-auth'] = myToken;
                    }
                }
                var myUrl = address + uri;
                var req = {
                    method: 'DELETE',
                    url: myUrl,
                    headers: myHeaders,
                    data: body
                }
                return $http(req);
            }


            sender.setToken = function(token){
                sender.token = token;
                $window.localStorage.setItem('token', token);
            }

            sender.getToken = function() {
                return sender.token;
            }

            sender.deleteToken = function() {
                sender.token = null;
                $window.localStorage.removeItem('token');
            }

        });