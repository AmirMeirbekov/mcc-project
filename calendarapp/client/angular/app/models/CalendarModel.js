/**
 * Created by Lukianchikov on 01-Nov-15.
 */
// This file describes the service (functionality shared with controllers). But we place it in models direcotry
//      simply because by convention if a service also provides any of data management (such as CRUD) functionalities,
//      we'd better call it a model.

angular.module('CalendarApp.Common')
    .service('CalendarModel',           // Let's make common service that deals with functions we use on Calendar Page.
        function ($http, ServersideConfigService, UtilsService) {
            var service = this,
                MODEL = "/calendar/";

            service.all = function () {     // Actual call to the server happens in here:
                return $http.get(ServersideConfigService.getUrl(    // it's $http.get(URL) style to conform to REST api
                    MODEL + ServersideConfigService.getCurrentFormat()))
                        .then(                // this is a 'promise', an asynchronuous processing of the result (an http answer)
                            function(result) {
                                return UtilsService.objectToArray(result);     // returned from server collection
                                            // basically we turn it to the array.
                            }
                        );
            };
//   REMAKE THESE::::::
             service.fetch = function (story_id) {     // Find and Load existing event by it id.
                return $http.get(
                    ServersideConfigService.getUrlForId(MODEL, story_id)
                );
            };

            service.create = function (story) {    // create new event
                return $http.post(
                    ServersideConfigService.getUrl(MODEL + ServersideConfigService.getCurrentFormat()), story
                );
            };

            service.update = function (story_id, story) {   // update existing event
                return $http.put(
                    ServersideConfigService.getUrlForId(MODEL, story_id), story
                );
            };

            service.destroy = function (story_id) {         // delete existing event
                return $http.delete(
                    ServersideConfigService.getUrlForId(MODEL, story_id)
                );
            };
    });