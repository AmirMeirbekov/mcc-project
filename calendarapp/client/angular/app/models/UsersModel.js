// This file describes the service (functionality shared with controllers). But we place it in models direcotry
//      simply because by convention if a service also provides any of data management (such as CRUD) functionalities,
//      we'd better call it a model.

angular.module('CalendarApp.Common')
    .service('UsersModel',                   // This service-model will provide functionality for controllers that
                                            // deal with CRUD of users.
    function (ServersideConfigService, UtilsService, SenderService, LoginService) {
        var service = this,
            MODEL = 'user/';

        service.all = function () {
            return SenderService.sendGet(ServersideConfigService.getUrl(MODEL + ServersideConfigService.getCurrentFormat()),{},{})
                    .then(
                        function(result) {
                            return UtilsService.objectToArray(result);          // show the array from users.
                        }
                    );
        };

        service.fetch = function (user_id) {
            return $http.get(ServersideConfigService.getUrlForId(MODEL, user_id));
        };

        service.create = function (user) {
            return SenderService.sendPost(ServersideConfigService.getUrl(MODEL + ServersideConfigService.getCurrentFormat()), user);
        };

        service.update = function (user, eMail) {
            return SenderService.sendPut(ServersideConfigService.getUrlForId(MODEL, user_id),{}, {});
            $http.put(ServersideConfigService.getUrlForId(MODEL, user_id), user);
        };

        service.destroy = function (user_id) {
            return $http.delete(ServersideConfigService.getUrlForId(MODEL, user_id));
        };
    });