var common = angular.module('CalendarApp.Common');

    common.controller("MessageCtrl", MessageCtrl);

    function MessageCtrl($scope) {
        var messageCtrl = this;

        messageCtrl.messageToSend = {
                            show: null,
                            class: "alert",
                            title: "Message:",
                            text: "Empty message here."
        }
        console.log("Message controller loaded...");
        $scope.$on('onCustomError', function(event, args) {   // on receiving error, print out message.
            console.log("PIECE " + args);
            messageCtrl.messageToSend.show = true;
            messageCtrl.messageToSend.class = "alert alert-danger fade in";
            messageCtrl.messageToSend.title = "Error:";
            messageCtrl.messageToSend.text = args;
            messageCtrl.message = messageCtrl.messageToSend;
            console.log("Message object: "+messageCtrl.message.show + messageCtrl.message.class + messageCtrl.message.title + messageCtrl.message.text);
        });

        $scope.$on('onCustomSuccess', function(event, args) {   // on receiving error, print out message.
            messageCtrl.messageToSend.show = true;
            messageCtrl.messageToSend.class = "alert alert-success fade in";
            messageCtrl.messageToSend.title = "Success:";
            messageCtrl.messageToSend.text = args;
            messageCtrl.message = messageCtrl.messageToSend;
        });

        $scope.$on('onCustomInfo', function(event, args) {   // on receiving error, print out message.
            messageCtrl.messageToSend.show = true;
            messageCtrl.messageToSend.class = "alert alert-info fade in";
            messageCtrl.messageToSend.title = "Info:";
            messageCtrl.messageToSend.text = args;
            messageCtrl.message = messageCtrl.messageToSend;
        });


        $scope.$on('$locationChangeStart', function() {
            messageCtrl.messageToSend.show = null;
            messageCtrl.messageToSend.class = "alert";
            messageCtrl.messageToSend.title = "Message:";
            messageCtrl.messageToSend.text = "Empty message here.";
            messageCtrl.message = messageCtrl.messageToSend;
        });

        messageCtrl.dismiss = function () {
            console.log("Dismiss pressed");
            messageCtrl.messageToSend.show = null;
            messageCtrl.messageToSend.class = "alert";
            messageCtrl.messageToSend.title = "Message:";
            messageCtrl.messageToSend.text = "Empty message here.";
            messageCtrl.message = messageCtrl.messageToSend;
            console.log("Message object: "+messageCtrl.message.show + messageCtrl.message.class + messageCtrl.message.title + messageCtrl.message.text);
        }
    };