/**
 * Created by Lukianchikov on 01-Nov-15.
 */
var common = angular.module('CalendarApp.Common');// Common Module that will be used by many other modules. It only deals with user auth.
    common.controller('MainCtrl', MainCtrl);


    function MainCtrl($scope, $location, LoginService, $rootScope, $timeout, $window) {
        var main = this;

        $scope.$on('$routeChangeStart', function (event, next, current) {
            var tmpToken = $window.localStorage.getItem('token');
            if (tmpToken != null){
                LoginService.setToken(tmpToken);
            }
            var tmp = $window.localStorage.getItem('loggedUser');
            if (tmp != null){
                main.loggedUser = tmp;
                console.log("loggedUser is "+tmp);
            }
            var tmp2 = $window.localStorage.getItem('currentUser');
            if (tmp2 != null){
                main.currentUser = tmp2;
                console.log("loggedUserName is "+tmp2);
            }
            var storedEmail = $window.localStorage.getItem('email');
            if (storedEmail != null){
                LoginService.setLoggedProfile(tmp2, storedEmail);
            }
        });

        $scope.$on('onCurrentUser', function(event, profile) {   // when onCurrentUser event happens in the scope of this
            //main.currentUser = LoginService.getUser();          // controller (it happens once user logs in),
            //console.log("On current user DETECTED! event object is "+ JSON.stringify(event));
            //console.log("On current user DETECTED! profile object is "+ JSON.stringify(profile));
            console.log("On current user DETECTED! Setting main.currentUser as "+ profile.username);
            $scope.currentUser = profile.username;      // register just logged in user in currentUser property.
            console.log("Profile:"+JSON.stringify(profile));
            console.log("Profile name:"+profile.username);
            main.loggedUser = true;
            $window.localStorage.setItem('loggedUser', true);
            $window.localStorage.setItem('currentUser', profile.username);
            $window.localStorage.setItem('email', profile.email);
        });

        main.logout = function() {                              // Function that will be called once user presses log out button.
            LoginService.logout();          // it calls LoginService function to perform log out.
            main.currentUser = null;                // Let's register in currentUser property that it is null now.
            main.loggedUser = null;
            $window.localStorage.removeItem('loggedUser');
            $window.localStorage.removeItem('currentUser');
            $window.localStorage.removeItem('email');
            console.log("LOGGED OUT SUCCESFULLY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            $location.path('/login');
            $timeout(function() {
                        $rootScope.$broadcast('onCustomInfo', "You have been logged out successfully.");
                        }, 2000);
        };
    };