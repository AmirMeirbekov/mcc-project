var userMdl = angular.module('CalendarApp.User');
    userMdl.controller('UserCtrl', userCtrl);                // don't confuse with UsersCtrl


function userCtrl ($window, $scope, $rootScope, LoginService, SenderService) {
            var userCtrl = this;
            $scope.username = "Mashka";
            $scope.email = "mashka@gmail.com";
            console.log("---> USER CONTROLLER SENDING HELLO!");

            userCtrl.refreshProfileInfo = function(){
                var userProfile = LoginService.getLoggedProfile();
                $scope.username = userProfile.username;
                $scope.email = userProfile.email;
            }

            userCtrl.refreshProfileInfo();

            $scope.$on('$routeChangeStart', function (event, next, current) {   // We need it if user refreshes the page
                userCtrl.refreshProfileInfo();
            });

            $scope.update = function (eMail){     // Function called from template to send changed email to server.
                console.log("--->Trying to update profile with new email:"+eMail);
                console.log("Token from Login.service:"+LoginService.token);
                var localProfile = $window.localStorage.getItem('profile');
                if (localProfile != null){
                    localProfile['email'] = eMail;
                    var dataObj = {email: eMail};
                    return SenderService.sendPut('/api/user/edit', {}, dataObj).then(function (val) {
                        var response = val.data;    // updating on the server returns updated values to client
                        $scope.username = response.username;
                        $scope.email = response.email;
                        var myEmail = $window.localStorage.getItem('email');
                        myEmail = response.email;
                        $window.localStorage.setItem('email', myEmail);
                        $rootScope.$broadcast('onCustomSuccess', "User has been updated successfully!");
                    }, function (err){   // or error.
                        $rootScope.$broadcast('onCustomError', err);
                    });
                }
            }
            /*
            myUser.getAssignedEvents = function (userId, events) {
                var assignedEvents
                Object.keys(events, function(key, value) {
                    if (value.assignee == userId) assignedEvents[key] = events[key];
                });

                return assignedEvents;
            };

            myUser.events = myUser.getAssignedEvents(myUser.userId, events);*/
        }