/**
 * Created by Lukianchikov on 01-Nov-15.
 */
// client/src/angello/Angello.js
var myModule = angular.module('CalendarApp',        // This is the Top-Level module. The configuration blocks of
    [                                               // all the dependencies is loaded before this top-module loaded).
        'ngRoute',
        'ngAnimate',
        'ngMessages',
        'CalendarApp.Common',
        'CalendarApp.Calendar',
        'CalendarApp.Events',
        'CalendarApp.Login',
        'CalendarApp.Signup',
        'CalendarApp.User'
    ]);
// src/angello/user/tmpl/user.html  -  no angello, and instead of src -- angular
// client/src/angello/Angello.js
myModule.config(function($routeProvider, $httpProvider) {
    $routeProvider
        .when('/', {    // root page -- a page showing a calendar.
            templateUrl: 'angular/calendar/tmpl/calendar.html',
            controller: 'CalendarCtrl',
            controllerAs: 'calendar'
        })
        .when('/events', {   // managing your events page
            templateUrl: 'angular/events/tmpl/events.html',
            controller: 'EventsCtrl',
            controllerAs: 'events'
        })
        //.when('/event', {   // creating event page
        //    templateUrl: 'angular/events/tmpl/event.html',
        //    controller: 'EventCtrl',
        //    controllerAs: 'event'
        //})
        //.when('/users', {   // seeing all the users page. It is for admin access
        //    templateUrl: 'angular/user/tmpl/users.html',
        //    controller: 'UsersCtrl',
        //    controllerAs: 'users'
        //})
            .when('/user', {  // a certain user profile page where you can change email address
            templateUrl: 'angular/user/tmpl/user.html',
            controller: 'UserCtrl',
            controllerAs: 'myUser'
        })
        .when('/login', {     // login page
            templateUrl: 'angular/login/tmpl/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
        .when('/signup', {     // signup page
            templateUrl: 'angular/signup/tmpl/signup.html',
            controller: 'SignupCtrl',
            controllerAs: 'signup'
        })
        .otherwise({redirectTo: '/'});

    $httpProvider.interceptors.push('authInterceptor');

    });

authInterceptor = function ($window, $rootScope, $location) { // used in every request to the server.
    // a hook that all other controllers will use to send a token in each HTTP request in the header.
    return {
        request: function(config){
            if ($window.localStorage.getItem('token') != null) {
                config.headers.Authorization = $window.localStorage.getItem('token');
            }
            else { //client doesn't have a token
                var desiredPath = $location.path();
                if (desiredPath != "/signup") {
                    $location.path('/login');
                }
            }
            return config;
        }
    }
}

myModule.factory('authInterceptor', authInterceptor);
/* Use this if you want to implement validation of whether user is authenticated when he changes pages.
myModule.run(function ($rootScope, LoginService) {

    $rootScope.$on('$locationChangeStart', function() {
      LoginService.authenticateUser();
    });
});
*/
