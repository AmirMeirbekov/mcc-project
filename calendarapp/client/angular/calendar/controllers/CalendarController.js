/**
 * Created by Lukianchikov on 01-Nov-15.
 */
// client/src/angello/storyboard/controllers/StoryboardController.js
var cal = angular.module('CalendarApp.Calendar');

cal.filter('inArray', function() {     // filter used in calendarDirective to filter dates that match exisitng events dates.
    return function(array, value) {
        var tmpDate = new Date(value)      ;
        var myDate = new Date(tmpDate.getFullYear(),tmpDate.getMonth(),tmpDate.getDate());
        for(var i = 0; i < array.length; i++){
            if(angular.equals(array[i].toISOString(), myDate.toISOString())){
                return true;
            }
    };
    return false;
    }
});

cal.controller("CalendarCtrl", function($scope, SenderService, $rootScope) {
    $scope.day = moment();

    var ctrl = this;
    $scope.events = [];       // stores list of all events locally. Used for iteration and finding todays events
    $scope.hour = 12;
    $scope.minute=15;
    $scope.priority=10;  // not used in controller. Just to set the initial values for template. Parameters for createFunction are taken from template, not from this variable (although it would be the same).
    $scope.availability="busy"; // not used in controller. Just to set the initial values for template.
    $scope.daysOfInterest = [];    // this holds days which should be highlighted on child directive calendar because they have some events

    $scope.enteredDay = Number(new Date(moment($scope.day)).getDate());
    $scope.enteredMonth = Number(new Date(moment($scope.day)).getMonth()+1);  // don't get deceived, enteredMonth doesn't refer to user enteredMonth. It is just shown to the user text area.
    $scope.enteredYear = Number(new Date(moment($scope.day)).getFullYear());
    console.log("INITIAL::: "+$scope.enteredYear+" "+$scope.enteredMonth+" "+$scope.enteredDay)

    $scope.eventExists = false;

    $scope.eventsToday = [];           // used in the template to show today events when we pick a date on the calendar.


    $scope.eventToEdit = {};
    $scope.eventToDelete = {};


    $scope.closeMyPopup = function () {  // function to close modal windows once user presses delete or edit button.
        $('#removeModal').modal('hide');
        $('#editModal').modal('hide');
    };


    ctrl.returnDay = function(dateObj){   // is used in template for edit form
        return Number(new Date(dateObj).getDate());
    }

    ctrl.returnMonth = function(dateObj){   // is used in template for edit form
        return Number(new Date(dateObj).getMonth()+1);
    }

    ctrl.returnYear = function(dateObj){   // is used in template for edit form
        return Number(new Date(dateObj).getFullYear());
    }

    ctrl.returnHour = function(dateObj){   // is used in template for edit form
        return Number(new Date(dateObj).getHours());
    }

    ctrl.returnMinute = function(dateObj){   // is used in template for edit form
        return Number(new Date(dateObj).getMinutes());
    }



    ctrl.matchDates = function(value1, value2){   // compare if DAYS are matching
        //first, strip off the time from both dates:
        console.log("STORED EVENT FULL DATE:"+value1);
        console.log("selected EVENT FULL DATE:"+value2);
        value1_noTime = new Date(value1.getFullYear(), value1.getMonth(), value1.getDate());
        value2_noTime = new Date(value2.getFullYear(), value2.getMonth(), value2.getDate());
        console.log("STRIPPED STORED EVENT FULL DATE:"+value1_noTime);
        console.log("stripped selected EVENT FULL DATE:"+value2_noTime);
        //console.log("DATE OF THE SELECTED DAY:"+value2_noTime);
        // now we can make them a ISO strings to compare
        if(angular.equals(value1_noTime.toISOString(), value2_noTime.toISOString())){
            //console.log(value1_noTime.toISOString()+" matches with "+value2_noTime.toISOString());
                return true;
        } else {
            //console.log(value1_noTime.toISOString()+" does not match with "+value2_noTime.toISOString());
            return false;
        }
    }

    $scope.$watch('day', function() {
        $scope.enteredDay = Number(new Date(moment($scope.day)).getDate());
        console.log("the day of selected day:"+$scope.enteredDay);
        $scope.enteredMonth = Number(new Date(moment($scope.day)).getMonth()+1);
        $scope.enteredYear = Number(new Date(moment($scope.day)).getFullYear());
        $scope.eventsToday = [];
        var matchedDates = false;
        $scope.eventExists = false;
        angular.forEach($scope.events, function(value, key) {
            var myDate = new Date(value.date);      // this is date from the local collection of events that should match database
           var systemFriendlyMonth = $scope.enteredMonth - 1;  // We use enteredMonth variable to show to user the month. Since in JavaScript month starts with 0, we had to add 1 to it. Now we need to take it back.
            var myIsoDate = new Date($scope.enteredYear, systemFriendlyMonth, $scope.enteredDay);  // this IsoDate stands for the date of the day that user clicks on the calendar.
            //console.log("FULL DATE of selected:"+myIsoDate);
            //console.log("FULL DATE of COMPARED to:"+myDate);
            matchedDates = ctrl.matchDates(myDate, myIsoDate);
            if (matchedDates == true){
                $scope.eventExists = true;
                $scope.eventsToday.push({"id": value._id, "name": value.name,
                    "date": value.date, "repeat": value.repeat, "sts": value.sts, "availability": value.availability,
                    "priority": value.priority, "loc": value.loc})
            }
        });
    });


    $scope.$watchCollection('events', function(){          // the same as watch, but it watches changes in collections
        angular.forEach($scope.events, function(value, key) {
              var dayWithEvents = new Date((new Date(value.date)).getFullYear(), (new Date(value.date)).getMonth(), (new Date(value.date)).getDate());      // we will take only day, month, year values
              var dayWithEvents2 = dayWithEvents.toISOString();             // fill in the events object.
              console.log("Day 1st type (goes to days with interest):"+dayWithEvents.getFullYear()+" "+dayWithEvents.getMonth()+" "+dayWithEvents.getDate());
              $scope.daysOfInterest.push(dayWithEvents);                  // no need to use ISO date.
    });
    });


    $scope.$watchCollection('eventToEdit', function(){
       // this tracks when user presses edit link on the event listed on the screen. We need to update variables for input form
       $scope.editId = $scope.eventToEdit.id;
        $scope.editName = $scope.eventToEdit.name;
        console.log("NAME: "+$scope.eventToEdit.name);
       $scope.editDay = ctrl.returnDay($scope.eventToEdit.date);
        console.log("edit DaTE:"+$scope.eventToEdit.date);
        console.log("edit DAY:"+$scope.editDay);
       $scope.editMonth = ctrl.returnMonth($scope.eventToEdit.date);
       $scope.editYear = ctrl.returnYear($scope.eventToEdit.date);
       $scope.editLoc = $scope.eventToEdit.loc;
        console.log("edit LOC:"+$scope.editLoc);
       $scope.editPriority = $scope.eventToEdit.priority;
       $scope.editRepeat = $scope.eventToEdit.repeat;
       $scope.editAvailability = $scope.eventToEdit.availability;
        console.log("edit AVAILABILITY:"+$scope.eventToEdit.availability);
       $scope.editHour = ctrl.returnHour($scope.eventToEdit.date);
       $scope.editMinute = ctrl.returnMinute($scope.eventToEdit.date);
    });


    ctrl.showEvents = function(){
           SenderService.sendGet('/api/events',{},{}).then(function(response){ // it gives us an object full of events
                angular.forEach(response.data, function(value, key) {            // fill in the events object.
                    console.log("THIS ADDED TO COLLECTION:"+JSON.stringify(value));
                    //console.log("VALUEEE_____E_E_E_E_E_E::"+JSON.stringify(value));
                    $scope.events.push(value);
                });   // save them locally to $scope.events.
               //$scope.highlight($scope.daysOfInterest);
               console.log("LIST OF EVENTS STORED::::::::: "+$scope.events);
           }, function(error){
                $rootScope.$broadcast("onCustomError","Error with loading events:"+error);
           });
    }
    ctrl.showEvents();


    $scope.createEvent = function(newName, newDay, newMonth, newYear, newLoc, newPriority, newRepeat, newAvailability, hour, minute){
        console.log("repeat to send:"+newRepeat);

        var myMonth = newMonth - 1;
        var myDate = new Date(newYear, myMonth, newDay);
        var now = new Date();
        if (myDate < now) {
            // selected date is in the past
            var status = "past";
        } else {
            var status = "upcoming";
        }
        if(!newRepeat){
            myRepeat = false;
        } else {
            myRepeat = newRepeat;
        }
        dataToSend = {
            "name": newName,
            "date": myDate.toISOString(),
            "time": hour.toString()+":"+minute.toString(),
            "loc": newLoc,
            "priority": newPriority,
            "repeat": myRepeat,
            "availability": newAvailability,
            "sts": status
        }
        console.log("SENDING to send:"+JSON.stringify(dataToSend));

        SenderService.sendPost('/api/events/new',{'Content-Type': 'application/json'},dataToSend).then(function(response){
                //console.log(JSON.stringify(response.data));
                $scope.closeMyPopup();
                $scope.events.push(response.data);
                //console.log("THIS ADDED TO COLLECTION:"+JSON.stringify(response.data));
                $rootScope.$broadcast("onCustomSuccess","Event successfully created!");
            }, function (error){
                $rootScope.$broadcast("onCustomError","Error occurs during creation of the event:"+error);
            });
    }


    $scope.editEvent = function(editId, editName, editDay, editMonth, editYear, editLoc, editPriority, editRepeat, editAvailability, editHour, editMinute) { // take all parameters from $scope.eventToEdit
      // params needed: eventToEdit.id, eventToEdit.name, editedDay, editedMonth, editedYear, editedLoc,
      // editedPriority, editedRepeat, editedAvailability, editedHour, editedMinute
        var myMonth =  editMonth - 1;
        console.log("UPDATING and the month is:"+myMonth);
        var myDate = new Date(editYear, myMonth, editDay);
        console.log("Date will be:"+ myDate);
        var now = new Date();
        if (myDate < now) {
            // selected date is in the past
            var status = "past";
        } else {
            var status = "upcoming";
        }
        var myRepeat = editRepeat;
        if(!myRepeat) {
            myRepeat = false;
        }

        dataToSend = {
            "name": editName,
            "date": myDate.toISOString(),
            "time": editHour.toString()+":"+editMinute.toString(),
            "loc": editLoc,
            "priority": editPriority,
            "repeat": myRepeat,
            "availability": editAvailability,
            "sts": status
        }
        console.log("SENDING to update:"+JSON.stringify(dataToSend));

        SenderService.sendPut('/api/events/'+$scope.editId+'/edit',{'Content-Type': 'application/json'},dataToSend).then(function(response){
                console.log("Server REsponded with data:"+JSON.stringify(response.data));
                var returnedId = response.data.id;
                $scope.closeMyPopup();
                // Let's take that updated event from server and add to our collection:
                $rootScope.$broadcast("onCustomSuccess","Event with id "+returnedId+" updated!");
                $scope.events = [];
                ctrl.showEvents();
            }, function (error){
                $scope.closeMyPopup();
                $rootScope.$broadcast("onCustomError","Error occurs during updating the event:"+error);
            });
    }


     $scope.removeEvent = function(myId){
         dataToSend = {
            "id": myId,
        }
        console.log("Deleting Event with ID:"+myId);
        SenderService.sendDel('/api/events/'+myId+'/edit',{'Content-Type': 'application/json'},dataToSend).then(function(response){
            $scope.closeMyPopup();
            $rootScope.$broadcast("onCustomSuccess","Event with id "+response.id +" has been deleted!");
            $scope.events = [];
            ctrl.showEvents();
        }, function(error){
            $scope.closeMyPopup();
            $rootScope.$broadcast("onCustomError","Error occurs trying to delete the event."+error);
        });
     }

});