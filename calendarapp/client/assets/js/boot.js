/**
 * Created by Lukianchikov on 01-Nov-15.
 */

// client/assets/js/boot.js
head.load(
// Handling messages, i.e. enable ngMessages:
    { file: '//code.jquery.com/jquery-1.11.0.min.js' },
    { file: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.3/angular.js' },
    { file: '//ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular-route.js'},
    { file: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.3/angular-messages.js' },
    //{ file: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.3/angular-route.min.js' },
    { file: '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.3/angular-animate.js' },
    { file: 'assets/js/bootstrap.js' },
    //{ file: 'https://cdn.rawgit.com/auth0/angular-jwt/master/dist/angular-jwt.js'},
    //{ file: 'assets/js/angular-storage.js'},
    { file: 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js'},
    { file: 'angular/app/App.js'},
    { file: 'angular/app/controllers/MainController.js'},
    { file: 'angular/app/controllers/MessageController.js'},
    { file: 'angular/app/models/CalendarModel.js'},
    { file: 'angular/app/models/UsersModel.js'},
    { file: 'angular/app/services/LoginService.js'},
    { file: 'angular/app/services/ServersideConfigService.js'},
    { file: 'angular/app/services/UtilsService.js'},
    { file: 'angular/app/services/SenderService.js'},
    { file: 'angular/calendar/Calendar.js'},
    { file: 'angular/calendar/controllers/CalendarController.js'},
    { file: 'angular/calendar/directives/CalendarDirective.js'},
    { file: 'angular/events/Events.js'},
    { file: 'angular/events/controllers/EventsController.js'},
    { file:'angular/login/Login.js'},
    { file:'angular/login/controllers/LoginController.js'},
    { file: 'angular/signup/Signup.js'},
    { file: 'angular/signup/controllers/SignupController.js'},
    { file: 'angular/user/User.js'},
    { file: 'angular/user/controllers/UserController.js'},
    { file: 'angular/user/controllers/UsersController.js'},
    { file: 'angular/CalendarApp.js'}
);
//...