// this file includes the user schema and its model
var db = require('./db');
var mongoose = require('mongoose');
// Now it creates Schema for the database:
var eventSchema = new mongoose.Schema({
  name: String,
  date: Date,
  user: String,
  loc: String,
  priority: Number,
  sts: String,
  repeat: Boolean,
  availability: String,
});
mongoose.model('Event', eventSchema); // We compiled eventSchema schema into the model called Event. We can use that
// string 'Event' (1st parameter) to pull the instance of the model later.
// We create documents(actual data) from models as: var docName = new ModelName(data)

module.exports = mongoose.model('Event', eventSchema);
