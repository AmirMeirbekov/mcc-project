// this file includes the user schema and its model
var db = require('./db');
var mongoose = require('mongoose');

// Now it creates Schema for the database:
var userSchema = new mongoose.Schema({
  username: {type: String, index: {unique: true}, required: true},
  password: {type: String, required: true, select: false},
  email: {type: String}
});

mongoose.model('User', userSchema); // We compiled eventSchema schema into the model called Event. We can use that

module.exports = mongoose.model('User', userSchema);